import sys
import time
from scipy import io
from FastEmbed import FastEmbedEig

if __name__ == '__main__':
    matVars = io.loadmat(sys.argv[1])['args']

    if 'lambdaMin' not in matVars.dtype.names:
        raise ValueError('Must specify the eigenvalue of the smallest eigenvector to embed lambdaMin')
    else:
        lambdaMin = matVars['lambdaMin'][0][0][0][0]

    if 'dProj' not in matVars.dtype.names:
        raise ValueError('Must specify the number of embedding dims dProj')
    else:
        dProj = matVars['dProj'][0][0][0][0]

    if 'lambdaMax' in matVars.dtype.names:
        lambdaMax = matVars['lambdaMax'][0][0][0][0]
    else:
        lambdaMax = 1.0

    fun = lambda x: x >= lambdaMin and x <= lambdaMax

    if 'spectralBound' in matVars.dtype.names:
        spectralBound = matVars['spectralBound'][0][0][0][0]
    else:
        spectralBound = None

    if 'njobs' in matVars.dtype.names:
        njobs = matVars['njobs'][0][0][0][0]
    else:
        njobs = None

    if 'polyOrder' in matVars.dtype.names:
        polyOrder = matVars['polyOrder'][0][0][0][0]
    else:
        polyOrder = None

    if 'boost' in matVars.dtype.names:
        boost = matVars['boost'][0][0][0][0]
    else:
        boost = None

    if 'polyType' in matVars.dtype.names:
        polyType = matVars['polyType'][0][0][0]
    else:
        polyType = 'legendre'

    t_start = time.time()
    embedMat = FastEmbedEig(mat=matVars['A'][0][0], fun=fun, polyOrder=polyOrder, boost=boost,
                            dProj=dProj, n_jobs=njobs, sigma_max=spectralBound, polyType=polyType)
    t_end = time.time()
    embed_time = t_end - t_start

    io.savemat(sys.argv[2], mdict={'Pts': embedMat.embed,'embed_time':embed_time})
