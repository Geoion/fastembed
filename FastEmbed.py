from multiprocessing import Pool, cpu_count
import numpy
from scipy import sparse
from scipy.integrate import quad
from scipy.constants import pi

SUPPORTEDPOLYTYPES = ['legendre', 'chebyshev']


def polyEval(r, x, polyType):
    """
        Evaluate (at -1<= x <= 1) the r-th order polynomial from the family specified by
        polyType
    """

    # invalid input
    if x < -1 or x > 1:
        raise ValueError("Polynomial series defined only in [-1, 1]")

    if polyType not in SUPPORTEDPOLYTYPES:
        raise ValueError("Error: Polynomial type not supported!\n" + \
                         "Polynomial types supported are: " + str(SUPPORTEDPOLYTYPES))

    # easy cases r = 0, 1:
    if r == 0:
        return 1
    if r == 1:
        return x

    # higher order r > 1
    # build from base case
    order = 1
    parent = float(x)
    grand_parent = 1.0

    if polyType == 'legendre':
        x_r = lambda r, x_r_minus_1, x_r_minus_2: \
            (2.0 - 1.0 / r) * x * x_r_minus_1 - (1.0 - 1.0 / r) * x_r_minus_2
    elif polyType == 'chebyshev':
        x_r = lambda r, x_r_minus_1, x_r_minus_2: \
            (2.0 - 1.0 * (r == 1)) * x * x_r_minus_1 - x_r_minus_2
    else:
        raise ValueError("Error: Polynomial type not supported!\n" + \
                         "Polynomial types supported are: " + str(SUPPORTEDPOLYTYPES))


    while order < r:
        # recursion
        order += 1
        current = x_r(order, parent, grand_parent)
        grand_parent = parent
        parent = current

    # exited loop for order = r
    # both current and parent hold poly(r,x)
    return parent


def polyEvalCum(coeff, x, polyType):
    """
    Computes all of the 0, ..., (len(coeff) - 1)-th order polynomials from the family polyType
    and returns their sum
    """

    # invalid input
    if x < -1 or x > 1:
        raise ValueError("Polynomial series defined only in [-1, 1]")

    if polyType not in SUPPORTEDPOLYTYPES:
        raise ValueError("Error: Polynomial type not supported!\n" +\
                    "Polynomial types supported are: " + str(SUPPORTEDPOLYTYPES))

    r = len(coeff) - 1
    # easy cases r = 0, 1:
    if r == 0:
        cumVal = coeff[0]
        return cumVal
    if r == 1:
        cumVal = coeff[0] + float(x) * coeff[1]
        return cumVal

    # higher order r > 1
    # build from base case
    order = 1
    parent = float(x)
    grand_parent = 1.0

    cumVal = coeff[0] + parent * coeff[1]

    if polyType == 'legendre':
        x_r = lambda r, x_r_minus_1, x_r_minus_2: \
            (2.0 - 1.0 / r) * x * x_r_minus_1 - (1.0 - 1.0/r) * x_r_minus_2
    elif polyType == 'chebyshev':
        x_r = lambda r, x_r_minus_1, x_r_minus_2: \
            (2.0 - 1.0 * (r == 1)) * x * x_r_minus_1 - x_r_minus_2
    else:
        raise ValueError("Error: Polynomial type not supported!\n" +\
                    "Polynomial types supported are: " + str(SUPPORTEDPOLYTYPES))
    
    # recursion
    while order < r:
        order += 1
        current = x_r(order, parent, grand_parent)
        cumVal += coeff[order] * current
        grand_parent = parent
        parent = current

    # exited loop for order = r
    return cumVal


def computeCoeffs(fun, L, polyType):
    """
        Expands fun(x) in using the first L polynomials from the family polyType
    """
    if polyType not in SUPPORTEDPOLYTYPES:
        raise ValueError("Error: Polynomial type not supported!\n" + \
                         "Polynomial types supported are: " + str(SUPPORTEDPOLYTYPES))

    coeff = []

    if polyType == 'legendre':
        for r in range(L + 1):
            intval, err = quad(lambda x: fun(x) * polyEval(r, x, polyType), -1, 1)
            coeff.append((float(r) + 0.5) * intval)
    elif polyType == 'chebyshev':
        for r in range(L + 1):
            intval, err = quad(lambda x: fun(x) * polyEval(r, x, polyType) \
                                         / (numpy.sqrt(1 - x * x)), -1, 1)
            coeff.append(intval / pi * 2.0)
        coeff[0] /= 2.0  # norm of 0-th order pol different

    return coeff


def projMat(args):
    """
    Core routine
    Input
        args = (A, sigma_max, embedMat, coeff, boost, polyType)
        A: square symmetric sparse scipy matrix
        sigma_max:  upper bound on spectral norm of A
        embedMat: projection matrix
        coeff: coeffs of polynomial expansion from family polyType
        boost: boosting parameter
        polyType: polynomial family
    Returns:
        embedding, a matrix of same size as embedMat
    Note:
        needs roughly 4*mem(embedding) extra memory for intermediate ops
    """

    A, sigma_max, embedMat, coeff, boost, polyType = args

    if not isinstance(A, sparse.csr_matrix):
        raise ValueError("Matrix must be a sparse CSR matrix")

    if A.shape[0] != A.shape[1]:
        raise ValueError("Matrix must be square")

    if A.shape[1] != embedMat.shape[0]:
        raise ValueError("Projection vectors must match matrix size")

    if boost <= 0:
        raise ValueError("boost must be a postive integer")

    if polyType not in SUPPORTEDPOLYTYPES:
        raise ValueError("Error: Polynomial type not supported!\n" +
                         "Polynomial types supported are: " + 
                         str(SUPPORTEDPOLYTYPES))

    # A_prime = A/sigma_max; A_prime is the matrix we use in the algorithm

    if polyType == 'legendre':
        x_r = lambda r, x_r_minus_1, x_r_minus_2: \
            ((2.0 - 1.0 / r) / sigma_max) * (A * x_r_minus_1) \
            - (1.0 - 1.0 / r) * x_r_minus_2
    elif polyType == 'chebyshev':
        x_r = lambda r, x_r_minus_1, x_r_minus_2: \
            ((2.0 - 1.0 * (r == 1))/sigma_max) * (A * x_r_minus_1) \
             - x_r_minus_2
    else:
        raise ValueError("Error: Polynomial type not supported!\n" +\
                    "Polynomial types supported are: " + str(SUPPORTEDPOLYTYPES))

    embedding = embedMat
    for boostIdx in range(boost):  # Boosting
        current_vectors = embedding
        parent_vectors = numpy.zeros(current_vectors.shape)
        embedding = coeff[0] * current_vectors

        for r in range(1, len(coeff)):
            grand_parent_vectors = parent_vectors
            parent_vectors = current_vectors
            current_vectors = x_r(r, parent_vectors, grand_parent_vectors)
            embedding += coeff[r] * current_vectors

    return embedding


def powerIter(args):
    """
        args = (A, vecs, iterMax)
        runs power iteration on the columns of vecs to estimate the spectral norm of A
        Input:
            A square sparse scipy matrix
            vecs: A numpy matrix of the same length as A
            iterMax: number of iterations
    """
    # unpack arguments
    A, vecs, iterMax = args
    k = vecs.shape[1]

    if A.shape[0] != A.shape[1]:
        raise ValueError("Matrix must be square")

    if A.shape[1] != vecs.shape[0]:
        raise ValueError("Starting vectors for power iteration must match matrix size")

    if iterMax <= 0:
        raise ValueError("Number of power iteration steps cannot be smaller than one")

    sigma_max_est = None

    for iterCount in range(iterMax):
        vecsNew = A * vecs
        vecsNewNorm = numpy.linalg.norm(vecsNew, axis=0)

        if iterCount == (iterMax - 1):
            vecsNorm = numpy.linalg.norm(vecs, axis=0)
            sigma_max_est = [y / x for (x, y) in zip(vecsNorm, vecsNewNorm)]

        vecs = vecsNew * sparse.csc_matrix(([1.0 / x for x in vecsNewNorm],
                                            (range(k), range(k))))

    if sigma_max_est is None:
        raise RuntimeError("Power iteration: spectral norm not estimated successfully")

    return sigma_max_est


class FastEmbedEig:
    def __init__(self, mat, fun, dProj=None, polyType='legendre', polyOrder=None,
                 boost=None, sigma_max=None, n_jobs=None, verbose=False):

        """
            Input:
                mat: A square symmetric sparse scipy matrix in CSR format
                fun: eigenvector weighting function (eigenvectors
                        are weighted as f(eigenvalue/sigma_max))
                dProj: dimensionality of embedding
                        Defaults to 6 log(n)
                polyOrder: Effective order of polynomial approximation
                        total number of serial steps
                boost: divides polynomial approximation into these many num of stages
                        Each stage uses a polyOrder/boost order approximation of fun
                        and these are cascaded
                sigma_max: spectral norm of mat (ideally); in any case this *should*
                        upper bound the spectral norm. If left empty an upper bound is
                        estimated on the fly
                n_jobs: number of workers to initialize. if this set to -1, we will use
                        total number of CPU cores available
                verbose: print some summary stats
                polyType:
                        Default is legendre (phi(x) = lambda x: 1.0).
                        Supported polytypes are specified in the GLOBAL variable SUPPORTEDPOLYTYPES (a dict)
            Useful fields:
                        Omega: JL lemma projection matrix used
                        fun: eigenvalue weighting function requested
                        funApprox: polynomial approximation of fun(x)
                                This is the eigenvalue weighting function that was
                                actually used

                        sigma_max: the upper bound on spectral norm (specified or estimated)
            Output field:
                        *** embed ***: The embedding
        """


        # n_jobs = -1: all available cores
        # Beware: if implemented locally, memory scales linearly with n_jobs

        if mat.shape[0] != mat.shape[1]:
            raise ValueError("Matrix must be square")

        if dProj is None:
            dProj = int(6 * numpy.log(mat.shape[0]))

        if dProj <= 0:
            raise ValueError("Dimensionality of the embedding must be a positive integer")

        if polyType not in SUPPORTEDPOLYTYPES:
            raise ValueError("Error: Polynomial type not supported!\n" +
                             "Polynomial types supported are: " + str(SUPPORTEDPOLYTYPES))
        if boost is None:
            boost = 2

        if boost <= 0:
            raise ValueError("Boosting parameter must be a positive integer")

        if polyOrder is None:
            polyOrder = 180

        if polyOrder < boost:
            raise ValueError("Polynomial order cannot be smaller than"
                             + " the boosting parameter")

        if n_jobs < -1 or n_jobs == 0:
            raise ValueError("Enter a valid number of processors: positive integers"
                             + " and -1 (all available CPUs)")

        # we need csr matrix for mat-vec multiplications
        if not isinstance(mat, sparse.csr_matrix):
            self.mat = sparse.csr_matrix(mat)
        else:
            self.mat = mat

        self.n = mat.shape[0]

        self.L = polyOrder // boost  # order of the polynomial approximation of fun(x) ** (1/boost)
        self.boost = boost  # number of repetitions
        self.dProj = dProj
        self.Omega = (2*numpy.random.randint(2, size=(self.n, dProj)) - 1) # / float(numpy.sqrt(dProj))
        self.polyOrder = self.L * boost
        self.verbose = verbose

        self.polyType = polyType
        self.coeff = computeCoeffs(lambda x: numpy.power(fun(x), 1.0 / boost), 
            self.L, polyType)
        self.fun = fun  # arguments to fun are relative to sigma_max
        self.funApprox = lambda x: polyEvalCum(self.coeff, x, self.polyType) ** boost

        # places an upper bound of ||A||_2 in self.sigma_max
        if sigma_max is None:
            n_parallel = dProj  # number of independent power iterations
            n_iter = 20  # number of iterations per PI
            scale_up = 1.01  # scale up lower bound of sigma_max for an upper_bound
            self.sigma_estimated = True
            self.__spectral_norm_est__(n_parallel, n_iter, scale_up, n_jobs)
        else:
            self.sigma_max = abs(float(sigma_max))
            self.sigma_estimated = False

        if verbose:
            if self.sigma_estimated:
                print "Estimated spectral norm is " + str(self.sigma_max)

        if verbose:
            print polyType + " coefficients (first 10) are " + str(self.coeff[:10])

        # generate the embedding and store it in *** embed ***
        # self.embed = projMat(self.mat, self.Omega.copy(), self.coeff, self.boost)
        self.__par__embed__(n_jobs)

        self.Omega /= float(numpy.sqrt(dProj))
        self.embed /= float(numpy.sqrt(dProj))

    def __spectral_norm_est__(self, k, niter, scale_up, n_jobs):
        """
            Sets up a parallel implementation of spectral norm estimation via Power Iteration
            using Python's multiprocessing library
        """

        if n_jobs is None:
            n_jobs = 1

        if n_jobs == -1:
            n_jobs = cpu_count()

        startVecs = numpy.random.randint(2, size=(self.n, k))

        n_jobs = min(n_jobs, k)

        if n_jobs == 1:
            # no need the over head of a new process
            self.sigma_max = powerIter((self.mat, startVecs, niter))
            if self.verbose:
                print "Estimated lower bounds on spectral norm " + \
                      str(self.sigma_max)
            self.sigma_max = scale_up * float(max(self.sigma_max))
            del startVecs
            return

        # create n_jobs processors
        pool = Pool(processes=n_jobs)

        # slice projection matrix (pick columns)

        eachDim = k // n_jobs
        indices = [range(r * eachDim, min((r + 1) * eachDim, k)) for r in range(n_jobs)]
        indices[-1].extend(range(eachDim * n_jobs, k))

        # different arguments for each processor
        args = [(self.mat, startVecs[:, indices[r]], niter) for r in range(n_jobs)]
        del startVecs, indices

        # map followed by reduce step (concatenate results together)
        self.sigma_max = numpy.concatenate(pool.map(powerIter, args), axis=1)

        pool.terminate()

        if self.verbose:
            print "Estimated lower bounds on spectral norm " + str(self.sigma_max)
        
        self.sigma_max = scale_up * float(max(self.sigma_max))

    def __par__embed__(self, n_jobs):
        """
            Sets up a parallel implementation of embedding algorithm using
            Python's multiprocessing library
        """

        if n_jobs is None:
            n_jobs = 1

        if n_jobs == -1:
            n_jobs = cpu_count()

        n_jobs = min(n_jobs, self.dProj)

        if n_jobs == 1:
            # no need the over head of a new process
            self.embed = projMat((self.mat, self.sigma_max, numpy.copy(self.Omega),
                                  self.coeff, self.boost, self.polyType))
            return

        # create n_jobs processors
        pool = Pool(processes=n_jobs)

        # slice projection matrix (pick columns)
        eachDim = self.dProj // n_jobs
        indices = [range(r * eachDim, min((r + 1) * eachDim, self.dProj))
                   for r in range(n_jobs)]
        indices[-1].extend(range(eachDim * n_jobs, self.dProj))

        # different arguments for each processor
        args = [(self.mat, self.sigma_max, self.Omega[:, indices[r]],
                 self.coeff, self.boost, self.polyType) for r in range(n_jobs)]

        # map followed by reduce step (concatenate results together)
        self.embed = numpy.concatenate(pool.map(projMat, args), axis=1)

        pool.terminate()


class FastEmbedSVD:
    """
        A wrapper class for SVD embeddings using FastEmbed
    """
    def __init__(self, mat, fun, dProj=None, polyType='legendre', polyOrder=None,
                 boost=None, sigma_max=None, n_jobs=None, verbose=False):
        """
            dProj defaults to 6 log(m+n)
            boost defaults to 2
            polyOrder defaults to 180
            sigma_max defaults to the spectral norm of mat
            n_jobs defaults to 1
        """
        self.mat = mat
        m = mat.shape[0]
        n = mat.shape[1]

        if boost is None:
            boost = 2

        if boost <= 0:
            raise ValueError("Boosting parameter must be a positive integer")

        if polyOrder is None:
            polyOrder = 180

        if polyOrder < boost:
            raise ValueError("Polynomial order cannot be smaller than"
                             + " the boosting parameter")

        # extra memory : 2*mem(mat)
        self.symmMat = sparse.vstack([sparse.hstack([sparse.csr_matrix((n, n)), mat.transpose(copy=True)]),
                                      sparse.hstack([mat, sparse.csr_matrix((m, m))])])

        if (polyOrder // boost) % 2 == 0:
            self.fun = lambda x: fun(x) * (x >= 0) + fun(-x) * (x < 0)
        else:
            self.fun = lambda x: fun(x) * (x >= 0) - fun(-x) * (x < 0)

        self.__eigEmbed__ = FastEmbedEig(self.symmMat, fun=self.fun, dProj=dProj,
                                         polyType=polyType,polyOrder=polyOrder, boost=boost,
                                         sigma_max=sigma_max, n_jobs=n_jobs, verbose=verbose)

        # free memory
        del self.symmMat

        self.embedRows = self.__eigEmbed__.embed[range(n, m + n), :]
        self.embedCols = self.__eigEmbed__.embed[range(n), :]

        # delete copy of embedding in FastEmbedEig object instance
        del self.__eigEmbed__.embed
