function [PtsRows, PtsCols] = FastEmbedSVDPy(args)

% Required params 
% 
%   args.A (sparse symmetric matrix) - input matrix
%   args.sigmaThresh (double) - singular vector with the lowest singular 
%               value that we wish to capture.
%               if args.sigmaMax is unspecified, this number is relative to 
%               maximum singular value of the input matrix args.A 
%               (lie in 0 to 1). An estimate of args.sigmaMax will be
%               computed and args.sigmaThresh will be scaled up by this
%               number
%   args.dProj (int) - number of embedding dimensions
% 
% Optional params
% 
%   args.polyOrder (int) (optional) - is the order of polynomial 
%               approximation Default 180
%   args.boost (int) (optional) is the number of times we cascade a polynomial 
%               approximation of thresholding function Default is 2
%               Note: effective polynomial approximation order is 
%               args.polyOrder/args.boost
%   args.polyType (str) (optional) - Can be one among 'legendre', 
%               'chebyshev'
%   args.sigmaMax (double) - an upper bound (expected to be tight) on the 
%               spectral norm of args.A (equivalently the max singular
%               value) we only embed singular vectors whose singular values 
%               are greater than args.sigmathresh/args.sigmaMax
%   args.njobs (int) (optional) - number of processes to use (Default 1)
%               and args.njobs = -1 means all available cores will be used
%   args.useSigma (bool) (optional)
%   if useSigma is true (Default) this corresponds to denoising-PCA embedding
%            i.e., f(sigma) = sigma * indicator(sigma > sigmaThresh)
%   if useSigma is false this corresponds to embeddings the leading
%   singular vectors (principal directions) with equal weights
%            i.e., f(sigma) = indicator(sigma > sigmaThresh)

tdir = tempdir;
if tdir(end) == '/', tdir = tdir(1:end-1); end
q = randi(1024,1,1); 
fname = [tdir,'/temp', num2str(q),'.mat']; clear tdir q;
save(fname); clear args;

thisFn = mfilename('fullpath');
i = length(thisFn);
c = thisFn(i);
while c~='/', i = i-1; c = thisFn(i); end
python([thisFn(1:i),'FastEmbedSVDFileIO.py'], fname, fname);

load(fname);
fprintf('Time taken inside Python %f seconds\n', embed_time);
PtsRows = double(PtsRows);
PtsCols = double(PtsCols);

% garbage collection
delete(fname);
