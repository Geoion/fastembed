# README #

### Summary ###

* Quick computation of spectral embedding of matrices 
* D. Ramasamy and U. Madhow. "Compressive spectral embedding: sidestepping the SVD," Neural Information Processing Systems 2015. http://arxiv.org/abs/1509.08360

### Setup ###

* Is built to run in Python 2.7 and needs the Scipy and NumPy libraries 
* Uses Python's multiprocessing library for leveraging the parallelism inherent in the embedding algorithm used

### Usage ###
* embedObj = FastEmbedEig(mat=A, fun=f, dProj=k, n_jobs=-1)
* k dimensional embedding of  A, a numpy/scipy sparse matrix, is available in embedObj.embed
* f specifies the spectral weighting function used. f(lambda) takes values in -1 <= lambda <= 1. Eigenvectors are scaled by f(lambda_i/spectralnorm(A)) and projected down to a k dimensional subspace (chosen at random). for example f = lambda x: abs(x) > 0.8, f = lambda x: x * (abs(x) > 0.8)
* n_jobs specifies the number of parallel processes to use. Setting n_jobs = -1 is translated to n_jobs=NUM_CPU_CORES
* For SVD embedding using the singular vector weighting function f(sigma/sigma_max), we use embedObj = FastEmbedSVD(mat=A, fun=f, dProj=k, n_jobs=-1). The embedding of rows and columns are available in embedObj.embedRows and embedObj.embedCols respectively
* A wrapper for use with MATLAB is available in FastEmbedEigPy.m and FastEmbedSVDPy.m