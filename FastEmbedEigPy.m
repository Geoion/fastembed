function Pts = FastEmbedEigPy(args)
% Required params 
% 
%   args.A (sparse symmetric matrix) - input matrix
%   args.lambdaMin (double) - eigenvector with the lowest eigenvalue we 
%               wish to capture (if args.spectralBound is unspecified this 
%               should lie in -1 to 1) and the algorithm will estimate
%               the spectral norm of A and scale up args.lambdaMin by the
%               spectral norm
%   args.dProj (int) - number of embedding dimensions
% 
% Optional params
% 
%   args.polyOrder (int) (optional) - is the order of polynomial
%               approximation Default 180
%   args.boost (int) (optional) is the number of times we cascade a polynomial
%               approximation of thresholding function Default is 2
%               Note: effective polynomial approximation order is 
%               args.polyOrder/args.boost
%   args.polyType (str) (optional) - Can be one among 'legendre', 
%               'chebyshev'
%   args.lambdaMax (double) (optional) - eigenvector with the largest that
%               eigenvalue we wish to capture (if args.spectralBound is 
%               unspecified this should lie in -1 to 1) and the algorithm 
%               will estimate the spectral norm of A and scale up
%               default is spectralBound
%               args.lambdaMax by the spectral norm
%   args.spectralBound (double) - an upper bound (expected to be tight) 
%               on the spectral norm of args.A 
%               we only embed eigenvectors whose eigenvalues are 
%               greater than args.lambdaMin and less than args.lambdaMax 
%               (if specified)
%   args.njobs (int) (optional) - number of processes to use (Default 1)
%               and args.njobs = -1 means all available cores will be used

tdir = tempdir;
if tdir(end) == '/', tdir = tdir(1:end-1); end
q = randi(1024,1,1);
fname = [tdir,'/temp', num2str(q),'.mat']; clear tdir;
save(fname); clear args;

thisFn = mfilename('fullpath');
i = length(thisFn);
c = thisFn(i);
while c~='/', i = i-1; c = thisFn(i); end
python([thisFn(1:i),'FastEmbedEigFileIO.py'],fname, fname);

load(fname);
fprintf('Time taken %f seconds\n', embed_time);
Pts = double(Pts);

% garbage collection
delete(fname);
