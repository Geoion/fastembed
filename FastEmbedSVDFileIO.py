import sys
import time
from scipy import io
from FastEmbed import FastEmbedSVD

if __name__ == '__main__':
    matVars = io.loadmat(sys.argv[1])['args']
    if 'dProj' not in matVars.dtype.names:
        raise ValueError('Must specify the number of embedding dims dProj')
    else:
        dProj = matVars['dProj'][0][0][0][0]

    if 'useSigma' in matVars.dtype.names:
        useSigma = matVars['useSigma'][0][0][0][0]
    else:
        useSigma = True

    if 'sigmaThresh' not in matVars.dtype.names:
        raise ValueError('Must specify threshold singular value sigmaThresh')
    else:
        sigmaThresh = matVars['sigmaThresh'][0][0][0][0]

    if useSigma:
        fun = lambda x: x * (x > matVars['sigmaThresh'])
    else:
        fun = lambda x: x > matVars['sigmaThresh']

    if 'sigmaMax' in matVars.dtype.names:
        sigmaMax = matVars['sigmaMax'][0][0][0][0]
        if sigmaMax <= 0:
            raise ValueError("sigmaMax must be a positive number")
    else:
        sigmaMax = None

    if 'njobs' in matVars.dtype.names:
        njobs = matVars['njobs'][0][0][0][0]
    else:
        njobs = None

    if 'polyOrder' in matVars.dtype.names:
        polyOrder = matVars['polyOrder'][0][0][0][0]
    else:
        polyOrder = None

    if 'boost' in matVars.dtype.names:
        boost = matVars['boost'][0][0][0][0]
    else:
        boost = None

    if 'polyType' in matVars.dtype.names:
        polyType = matVars['polyType'][0][0][0]
    else:
        polyType = 'legendre'

    t_start = time.time()
    embedMat = FastEmbedSVD(mat=matVars['A'][0][0], fun=fun, polyOrder=polyOrder, boost=boost,
                            dProj=dProj, n_jobs=njobs, sigma_max=sigmaMax, polyType=polyType)

    t_end = time.time()
    embed_time = t_end - t_start
    sigma_max = embedMat.__eigEmbed__.sigma_max

    io.savemat(sys.argv[2], mdict={'PtsRows': embedMat.embedRows, 'PtsCols': embedMat.embedCols,
                                   'embed_time': embed_time, 'sigma_max': sigma_max})
